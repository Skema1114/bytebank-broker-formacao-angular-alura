# BytebankBrokerFormacaoAngularAlura

## Neste projeto:

- Curso:

  - Angular e RxJS: Programação reativa
  - https://cursos.alura.com.br/course/angular-rxjs-introducao-programacao-reativa

## Comandos utilizados

- ng g i acoes/modelo/acoes
- ng g s acoes/acoes

## Anotações

## Entendendo

- Observable
  | TIPO | VALOR ÚNICO | COLEÇÃO DE VALORES |
  |------------|-------------|--------------------|
  | Imperativo | Função | Iterator |
  | Reativo | Promise | Observable |
